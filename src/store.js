import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import store from 'store';

import { CONTROL_LANGUAGE, GENERIC_ID } from 'actions/controlActions';
import { normalize } from 'redux-shelf';

import appReducer from './reducers';

/* eslint-disable-next-line no-underscore-dangle */
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const language = store.get('language');
let preloadedState = {};

if (language) {
  preloadedState = {
    entities: { [CONTROL_LANGUAGE]: normalize({ code: language, id: GENERIC_ID }) },
  };
}

export default createStore(
  appReducer,
  preloadedState,
  composeEnhancers(applyMiddleware(thunk)),
);
