import React from 'react';
import { Provider } from 'react-redux';
import { HashRouter as Router } from 'react-router-dom';
import { Layout } from 'antd';

import { FormattedMessage } from 'react-intl';

import LocaleProvider from 'components/shared/LocaleProvider'; // eslint-disable-line import/no-named-as-default
import Header from 'components/shared/Header';

import store from './store';
import Routes from './Routes';

import styles from './App.css';

const {
  Content,
  Footer,
} = Layout;

const App = () => (
  <Provider store={store}>
    <LocaleProvider>
      <Router>
        <Layout className={styles.app}>
          <Header />
          <Content className={styles.content}>
            <Routes />
          </Content>
          <Footer className={styles.footer}>
            <FormattedMessage id="footer" values={{ name: 'José Sergio Mendes Pereira Junior' }} />
          </Footer>
        </Layout>
      </Router>
    </LocaleProvider>
  </Provider>
);

export default App;
