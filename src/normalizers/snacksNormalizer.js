import { normalizeIngredients } from 'normalizers/ingredientsNormalizer';

const normalizeSnacks = snacks =>
  snacks.reduce((acc, curr) => {
    const snack = {
      name: curr.nome,
      ingredients: normalizeIngredients(curr.ingredientes),
    };

    return acc.concat(snack);
  }, []);

export default normalizeSnacks;
