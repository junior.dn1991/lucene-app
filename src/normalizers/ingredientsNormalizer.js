export const normalizeIngredients = (ingredients, category = '') =>
  ingredients.reduce(
    (acc, ingredient) =>
      acc.concat({
        name: ingredient.nome,
        value: ingredient.valor,
        category: ingredient.categoria || category,
      }),
    [],
  );

export const normalizeIngredientsByCategories = categories =>
  categories.reduce((acc, category) => {
    const ingredients = normalizeIngredients(category.ingredientes, category.categoria);

    return acc.concat(ingredients);
  }, []);
