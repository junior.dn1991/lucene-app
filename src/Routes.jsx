import React from 'react';
import { Route, Switch } from 'react-router-dom';
import PropTypes from 'prop-types';

import MyLoadable from './components/shared/MyLoadable';

import AppliedRoute from './components/shared/AppliedRoute';

const AsyncOrderPage = MyLoadable({
  loader: /* istanbul ignore next */ () => import('./containers/OrderPage'),
});

const AsyncNotFound = MyLoadable({
  loader: /* istanbul ignore next */ () => import('./containers/NotFound'),
});

const Routes = ({ childProps }) => (
  <Switch>
    <AppliedRoute
      path="/"
      exact
      component={AsyncOrderPage}
      props={childProps}
    />
    <Route component={AsyncNotFound} />
  </Switch>
);

Routes.propTypes = {
  childProps: PropTypes.shape({}),
};

Routes.defaultProps = {
  childProps: {},
};

export default Routes;
