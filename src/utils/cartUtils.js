export function calculateSnackValue(snack, ingredients) {
  return ingredients.reduce((acc, ingredient) => {
    if (snack.typeOfBread && snack.typeOfBread === ingredient.name) {
      return acc + parseFloat(ingredient.value);
    }

    if (snack.cheese && snack.cheese === ingredient.name) {
      return acc + parseFloat(ingredient.value);
    }

    if (snack.filling && snack.filling === ingredient.name) {
      return acc + parseFloat(ingredient.value);
    }

    if (snack.salad.includes(ingredient.name)) {
      const multiplier = snack.doubleSalad.includes(ingredient.name) ? 2 : 1;
      return acc + parseFloat(ingredient.value * multiplier);
    }

    if (snack.sauces.includes(ingredient.name)) {
      const multiplier = snack.doubleSauces.includes(ingredient.name) ? 2 : 1;
      return acc + parseFloat(ingredient.value * multiplier);
    }

    if (snack.spice.includes(ingredient.name)) {
      const multiplier = snack.doubleSpice.includes(ingredient.name) ? 2 : 1;
      return acc + parseFloat(ingredient.value * multiplier);
    }

    return acc;
  }, 0);
}

export function calculateTotal(snacks, ingredients) {
  return snacks.reduce((acc, curr) => acc + calculateSnackValue(curr, ingredients), 0);
}

function normalizeSnackIngredientsToPayload(snack, ingredients) {
  return ingredients.reduce((acc, ingredient) => {
    const ingredientNormalized = {
      nome: ingredient.name,
      categoria: ingredient.category,
      valor: ingredient.value,
    };

    if (snack.typeOfBread && snack.typeOfBread === ingredient.name) {
      return acc.concat(ingredientNormalized);
    }

    if (snack.cheese && snack.cheese === ingredient.name) {
      return acc.concat(ingredientNormalized);
    }

    if (snack.filling && snack.filling === ingredient.name) {
      return acc.concat(ingredientNormalized);
    }

    if (snack.salad.includes(ingredient.name)) {
      if (snack.doubleSalad.includes(ingredient.name)) {
        return acc.concat(ingredientNormalized, ingredientNormalized);
      }
      return acc.concat(ingredientNormalized);
    }

    if (snack.sauces.includes(ingredient.name)) {
      if (snack.doubleSauces.includes(ingredient.name)) {
        return acc.concat(ingredientNormalized, ingredientNormalized);
      }
      return acc.concat(ingredientNormalized);
    }

    if (snack.spice.includes(ingredient.name)) {
      if (snack.doubleSpice.includes(ingredient.name)) {
        return acc.concat(ingredientNormalized, ingredientNormalized);
      }
      return acc.concat(ingredientNormalized);
    }

    return acc;
  }, []);
}

export function normalizeSnacksToPayload(snacks, ingredients) {
  return snacks.reduce((acc, curr) => {
    const snack = {
      nome: curr.name,
      ingredientes: normalizeSnackIngredientsToPayload(curr, ingredients),
    };
    return acc.concat(snack);
  }, []);
}

function normalizeSplit(value = 0, percent = 0) {
  return { valor: parseFloat(value).toFixed(2), percentual: parseFloat(percent).toFixed(2) };
}

export function normalizeSplits(additionalInformation) {
  if (!additionalInformation.amount_of_people || additionalInformation.amount_of_people === 1) {
    return [normalizeSplit(additionalInformation.valueOfOrder, 100)];
  }

  const splittingValues = additionalInformation.calculator
    .filter(split => split.percent || split.value);

  if (splittingValues.length === additionalInformation.calculator.length) {
    return splittingValues.map(split => normalizeSplit(split.value, split.percent));
  }

  let totalOfSplitsValue = 0;
  const splits = splittingValues.map((split) => {
    totalOfSplitsValue = parseFloat(totalOfSplitsValue + split.value);
    return normalizeSplit(split.value, split.percent);
  });

  const peopleSplitMissing = additionalInformation.calculator.length - splittingValues.length;

  const proportionalValuePerPeople =
    parseFloat((additionalInformation.valueOfOrder - totalOfSplitsValue) / peopleSplitMissing);

  const proportionalPercentPerPeople =
    parseFloat((proportionalValuePerPeople / additionalInformation.valueOfOrder) * 100);

  const missingSplits = [...Array(peopleSplitMissing).keys()]
    .map(() => normalizeSplit(proportionalValuePerPeople, proportionalPercentPerPeople));

  return splits.concat(missingSplits);
}
