import { combineReducers } from 'redux';

import { entities, communication } from 'redux-shelf';

export default combineReducers({
  communication,
  entities,
});
