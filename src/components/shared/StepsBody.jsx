import React from 'react';
import PropTypes from 'prop-types';

import styles from './StepsBody.css';

const StepsBody = ({ children }) => <div className={styles.stepsContent}>{children}</div>;

StepsBody.propTypes = {
  children: PropTypes.node,
};

StepsBody.defaultProps = {
  children: null,
};

export default StepsBody;
