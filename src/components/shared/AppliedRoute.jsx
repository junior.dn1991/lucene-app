import React from 'react';
import { Route } from 'react-router-dom';
import PropTypes from 'prop-types';

const AppliedRoute = ({ component: Component, props: childProps, ...rest }) =>
  <Route {...rest} render={props => <Component {...props} {...childProps} />} />;

AppliedRoute.propTypes = {
  component: PropTypes.oneOfType([PropTypes.node, PropTypes.func]),
  props: PropTypes.shape({}),
};

AppliedRoute.defaultProps = {
  component: null,
  props: {},
};

export default AppliedRoute;
