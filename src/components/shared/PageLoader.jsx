import React from 'react';
import PropTypes from 'prop-types';

import { Spin, Icon } from 'antd';
import { FormattedMessage } from 'react-intl';

import styles from './PageLoader.css';

const PageLoader = ({ isLoading, error }) => {
  if (isLoading) {
    return (
      <div className={styles.overlay}>
        <Spin indicator={<Icon type="loading" className={styles.loader} spin />} />
      </div>
    );
  }

  if (error) {
    return <div><FormattedMessage id="page_loading_error" /></div>;
  }

  return null;
};

PageLoader.propTypes = {
  isLoading: PropTypes.bool,
  error: PropTypes.oneOfType([PropTypes.bool, PropTypes.shape({})]),
};

PageLoader.defaultProps = {
  isLoading: false,
  error: false,
};

export default PageLoader;
