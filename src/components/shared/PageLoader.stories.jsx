import React from 'react';
import { storiesOf } from '@storybook/react';
import PageLoader from './PageLoader';

import { LocaleProvider } from './LocaleProvider';

storiesOf('PageLoader', module)
  .add('default', () => <LocaleProvider><PageLoader /></LocaleProvider>)
  .add('loading', () => <LocaleProvider><PageLoader isLoading /></LocaleProvider>)
  .add('with error', () => <LocaleProvider><PageLoader error={{ message: 'error' }} /></LocaleProvider>);

