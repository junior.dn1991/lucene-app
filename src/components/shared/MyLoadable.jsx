import Loadable from 'react-loadable';
import PageLoader from './PageLoader';

const MyLoadable = opts =>
  Loadable({
    loading: PageLoader,
    timeout: 10,
    delay: 300,
    ...opts,
  });

export default MyLoadable;
