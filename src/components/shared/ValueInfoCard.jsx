import React from 'react';
import PropTypes from 'prop-types';

import { Card } from 'antd';

import styles from './ValueInfoCard.css';

const ValueInfoCard = ({ decimal, value }) => (
  <Card className={styles.container}>
    R$ {decimal ? value.toFixed(2) : value }
  </Card>
);

ValueInfoCard.propTypes = {
  decimal: PropTypes.bool,
  value: PropTypes.number,
};

ValueInfoCard.defaultProps = {
  decimal: false,
  value: 0,
};

export default ValueInfoCard;
