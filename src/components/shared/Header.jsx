import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import cx from 'classnames';

import { Icon, Layout } from 'antd';

import ReactCountryFlag from 'react-country-flag';

import { FormattedMessage } from 'react-intl';

import { CONTROL_LANGUAGE, changeLanguage, GENERIC_ID } from 'actions/controlActions';

import { PT_BR, EN_US } from 'constants/locales';

import styles from './Header.css';

const {
  Header: AntdHeader,
} = Layout;

const Header = ({ onChangeLanguage, language }) => (
  <AntdHeader>
    <FormattedMessage id="choose_your_language" />
    <Icon
      className={cx([
        styles.flags,
        { [styles.inactiveFlag]: language.code !== EN_US.code },
      ])}
      onClick={() => onChangeLanguage(EN_US.code)}
    >
      <ReactCountryFlag code={EN_US.code} svg />
    </Icon>
    <Icon
      className={cx([
        styles.flags,
        { [styles.inactiveFlag]: language.code !== PT_BR.code },
      ])}
      onClick={() => onChangeLanguage(PT_BR.code)}
    >
      <ReactCountryFlag code={PT_BR.code} svg />
    </Icon>
  </AntdHeader>
);

Header.propTypes = {
  onChangeLanguage: PropTypes.func,
  language: PropTypes.shape({
    code: PropTypes.string,
  }),
};

Header.defaultProps = {
  onChangeLanguage: () => {},
  language: {
    code: PT_BR.code,
  },
};

export default connect(
  ({ entities }) => ({ language: entities.contentOf(CONTROL_LANGUAGE, GENERIC_ID) }),
  { onChangeLanguage: changeLanguage },
)(Header);
