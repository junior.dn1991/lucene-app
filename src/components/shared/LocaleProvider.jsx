import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { LocaleProvider as AntdLocaleProvider } from 'antd';
import { IntlProvider } from 'react-intl';

import { PT_BR, EN_US } from 'constants/locales';
import { CONTROL_LANGUAGE, GENERIC_ID } from 'actions/controlActions';

import AntdPtBR from 'antd/lib/locale-provider/pt_BR';
import AntdEnUS from 'antd/lib/locale-provider/en_US';

import enUsMessages from 'locales/en_US.json';
import ptBrMessages from 'locales/pt_BR.json';

export const LocaleProvider = ({ children, language }) => {
  let messages = ptBrMessages;
  let antdLocale = AntdPtBR;

  if (language.code === EN_US.code) {
    messages = enUsMessages;
    antdLocale = AntdEnUS;
  }

  return (
    <AntdLocaleProvider locale={antdLocale}>
      <IntlProvider locale="en" messages={messages}>
        {children}
      </IntlProvider>
    </AntdLocaleProvider>
  );
};

LocaleProvider.propTypes = {
  children: PropTypes.node,
  language: PropTypes.shape({
    code: PropTypes.string,
  }),
};

LocaleProvider.defaultProps = {
  children: null,
  language: {
    code: PT_BR.code,
  },
};

export default connect(({ entities }) =>
  ({ language: entities.contentOf(CONTROL_LANGUAGE, GENERIC_ID) }))(LocaleProvider);
