import React from 'react';
import { storiesOf } from '@storybook/react';
import StepsBody from './StepsBody';

storiesOf('StepsBody', module)
  .add('without children', () => <StepsBody />)
  .add('with children', () => <StepsBody><h1>children</h1></StepsBody>);
