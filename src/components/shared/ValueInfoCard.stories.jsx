import React from 'react';
import { storiesOf } from '@storybook/react';
import ValueInfoCard from './ValueInfoCard';

storiesOf('ValueInfoCard', module)
  .add('default', () => <ValueInfoCard />)
  .add('default with decimal', () => <ValueInfoCard decimal />)
  .add('with value', () => <ValueInfoCard value={8000} />)
  .add('with decimal value', () => <ValueInfoCard value={8000} decimal />);
