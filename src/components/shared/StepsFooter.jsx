import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'antd';

import { FormattedMessage } from 'react-intl';

import styles from './StepsFooter.css';

const StepsFooter = ({
  current,
  onDone,
  onNext,
  onPrevious,
  steps,
}) => (
  <div className={styles.actions}>
    {
      current < steps - 1
      &&
      <Button type="primary" onClick={() => onNext()}>
        <FormattedMessage id="steps_next" />
      </Button>
    }
    {
      current === steps - 1
      &&
      <Button
        type="primary"
        onClick={() => {
          onDone();
        }}
      >
        <FormattedMessage id="steps_done" />
      </Button>
    }
    {
      current > 0
      &&
      <Button className={styles.previous} onClick={() => onPrevious()}>
        <FormattedMessage id="steps_previous" />
      </Button>
    }
  </div>
);

StepsFooter.propTypes = {
  current: PropTypes.number,
  onDone: PropTypes.func,
  onNext: PropTypes.func,
  onPrevious: PropTypes.func,
  steps: PropTypes.number,
};

StepsFooter.defaultProps = {
  current: 0,
  onDone: () => {},
  onNext: () => {},
  onPrevious: () => {},
  steps: 0,
};

export default StepsFooter;
