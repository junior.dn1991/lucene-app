export const INGREDIENT_CATEGORY_BREAD = 'Tipo de pão';
export const INGREDIENT_CATEGORY_CHEESE = 'Queijos';
export const INGREDIENT_CATEGORY_FILLING = 'Recheio';
export const INGREDIENT_CATEGORY_SAUCES = 'Molhos';
export const INGREDIENT_CATEGORY_SALAD = 'Saladas';
export const INGREDIENT_CATEGORY_SPICE = 'Temperos';
