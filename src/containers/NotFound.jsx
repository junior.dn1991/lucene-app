import React from 'react';
import { FormattedMessage } from 'react-intl';

const NotFound = () => <div><h3><FormattedMessage id="page_not_found" /></h3></div>;

export default NotFound;
