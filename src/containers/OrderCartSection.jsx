import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import cx from 'classnames';

import {
  Button,
  Divider,
  Icon,
  message,
  Table,
  Tooltip,
  Tag,
} from 'antd';

import { injectIntl, FormattedMessage } from 'react-intl';

import {
  addSnackToCart,
  removeSnackOfCart,
  CONTROL_ORDER_CART,
} from 'actions/controlActions';

import { calculateSnackValue } from 'utils/cartUtils';

import ValueInfoCard from 'components/shared/ValueInfoCard';

import StepsBody from 'components/shared/StepsBody';
import StepsFooter from 'components/shared/StepsFooter';
import CartModalForm from './CartModalForm';

import styles from './OrderCartSection.css';

const { Column } = Table;

class OrderCartSection extends Component {
  static propTypes = {
    intl: PropTypes.shape({
      formatMessage: PropTypes.func,
    }),
    loading: PropTypes.bool,
    onCloneSnack: PropTypes.func,
    onRemoveSnack: PropTypes.func,
    stepsProps: PropTypes.shape({
      onNext: PropTypes.func,
      onPrevious: PropTypes.func,
    }),
    snacks: PropTypes.arrayOf(PropTypes.shape({})),
    totalOfOrder: PropTypes.number,
  }

  static defaultProps = {
    intl: {
      formatMessage: () => {},
    },
    loading: false,
    onCloneSnack: () => {},
    onRemoveSnack: () => {},
    stepsProps: {
      onNext: () => {},
      onPrevious: () => {},
    },
    snacks: [],
    totalOfOrder: 0,
  }

  state = {
    showModal: false,
    editingSnack: {},
  }

  handleShowModal = () => {
    this.setState({ showModal: true });
  }

  handleCloseModal = () => {
    this.setState({ showModal: false, editingSnack: {} });
  }

  handleNext = () => {
    const { snacks, stepsProps, intl } = this.props;
    const { onNext } = stepsProps;
    if (!snacks.length) {
      message.error(intl.formatMessage({ id: 'empty_cart_message' }));
      return;
    }
    onNext();
  }

  handlePrevious = () => {
    const { onPrevious } = this.props.stepsProps;
    onPrevious();
  }

  handleEditSnack = snack => this.setState({ showModal: true, editingSnack: snack });

  render() {
    const {
      loading,
      onCloneSnack,
      onRemoveSnack,
      snacks,
      stepsProps,
      totalOfOrder,
    } = this.props;

    const { showModal, editingSnack } = this.state;

    return (
      <Fragment>
        <StepsBody>
          <div className={styles.tableOperations}>
            <Button type="primary" onClick={this.handleShowModal}>
              <FormattedMessage id="add_snack" />
            </Button>
          </div>
          <Table
            dataSource={snacks}
            bordered
            pagination={false}
            loading={loading}
            rowKey={snack => snack.id}
          >
            <Column
              title={<FormattedMessage id="snack_name" />}
              dataIndex="name"
              key="name"
              render={(value, snack) => {
                if (value) {
                  return value;
                }

                return `${snack.typeOfBread} ${snack.filling || ''}`;
              }}
            />
            <Column
              title={<FormattedMessage id="type_of_bread" />}
              dataIndex="typeOfBread"
              key="typeOfBread"
            />
            <Column
              title={<FormattedMessage id="cheese" />}
              dataIndex="cheese"
              key="cheese"
            />
            <Column
              title={<FormattedMessage id="filling" />}
              dataIndex="filling"
              key="filling"
            />
            <Column
              title={<FormattedMessage id="salad" />}
              dataIndex="salad"
              key="salad"
              render={values => (
                <div className={styles.tags}>
                  {values.map(value => <Tag color="green" key={value}><FormattedMessage id={value} /></Tag>)}
                </div>
              )}
            />
            <Column
              title={<FormattedMessage id="sauces" />}
              dataIndex="sauces"
              key="sauces"
              render={values => (
                <div className={styles.tags}>
                  {values.map(value => <Tag color="orange" key={value}><FormattedMessage id={value} /></Tag>)}
                </div>
              )}
            />
            <Column
              title={<FormattedMessage id="spices" />}
              dataIndex="spice"
              key="spice"
              render={values => (
                <div className={styles.tags}>
                  {values.map(value => <Tag color="purple" key={value}><FormattedMessage id={value} /></Tag>)}
                </div>
              )}
            />
            <Column
              title={<FormattedMessage id="snack_value" />}
              dataIndex="value"
              key="value"
              render={value => `R$ ${value.toFixed(2)}`}
            />
            <Column
              title={<FormattedMessage id="actions" />}
              key="action"
              align="center"
              width={150}
              render={snack => (
                <div className={styles.icons}>
                  <Tooltip title={<FormattedMessage id="tooltip_clone" />}>
                    <Icon
                      className={cx(styles.cursorPointer, styles.cloneIcon)}
                      type="copy"
                      onClick={() => onCloneSnack(snack)}
                    />
                  </Tooltip>
                  <Divider type="vertical" className={styles.divider} />
                  <Tooltip title={<FormattedMessage id="tooltip_edit" />}>
                    <Icon
                      className={styles.cursorPointer}
                      type="edit"
                      onClick={() => this.handleEditSnack(snack)}
                    />
                  </Tooltip>
                  <Divider type="vertical" className={styles.divider} />
                  <Tooltip title={<FormattedMessage id="tooltip_delete" />}>
                    <Icon
                      className={cx(styles.cursorPointer, styles.removeIcon)}
                      type="delete"
                      onClick={() => onRemoveSnack(snack.id)}
                    />
                  </Tooltip>
                </div>
              )}
            />
          </Table>
          <ValueInfoCard decimal value={totalOfOrder} />
        </StepsBody>
        <StepsFooter
          {...stepsProps}
          onNext={this.handleNext}
          onPrevious={this.handlePrevious}
        />
        <CartModalForm
          showModal={showModal}
          onCloseModal={this.handleCloseModal}
          editingSnack={editingSnack}
        />
      </Fragment>
    );
  }
}

export default connect(
  ({ entities, communication }) => {
    const {
      loading,
      error,
    } = communication.of(CONTROL_ORDER_CART);

    let totalOfOrder = 0;

    const ingredientIds = entities.idsOf('ingredients');
    const ingredients = ingredientIds.map(ingredientId => entities.contentOf('ingredients', ingredientId));

    const snackIds = entities.idsOf(CONTROL_ORDER_CART);
    const snacks = snackIds.map((snackId) => {
      const snack = entities.contentOf(CONTROL_ORDER_CART, snackId);
      const value = calculateSnackValue(snack, ingredients);
      totalOfOrder += value;

      return { ...snack, value };
    });

    return {
      loading,
      error,
      snacks,
      totalOfOrder,
    };
  },
  {
    onCloneSnack: addSnackToCart,
    onRemoveSnack: removeSnackOfCart,
  },
)(injectIntl(OrderCartSection));
