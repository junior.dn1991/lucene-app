import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import {
  Avatar,
  Card,
  Col,
  Divider,
  Form,
  InputNumber,
  message,
  Radio,
  Row,
} from 'antd';

import { injectIntl, FormattedMessage } from 'react-intl';

import cx from 'classnames';

import {
  CONTROL_ORDER_CART,
  CONTROL_ORDER_INFO,
  GENERIC_ID,
  sendOrder,
} from 'actions/controlActions';

import { calculateTotal } from 'utils/cartUtils';

import StepsBody from 'components/shared/StepsBody';
import StepsFooter from 'components/shared/StepsFooter';

import styles from './OrderReviewSection.css';

const FormItem = Form.Item;
const RadioGroup = Radio.Group;

class OrderReviewSection extends Component {
  static propTypes = {
    intl: PropTypes.shape({
      formatMessage: PropTypes.func,
    }),
    form: PropTypes.shape({}),
    stepsProps: PropTypes.shape({
      onNext: PropTypes.func,
      onPrevious: PropTypes.func,
    }),
    orderInfo: PropTypes.shape({
      name: PropTypes.string,
      address: PropTypes.string,
    }),
    valueOfOrder: PropTypes.number,
    onSendOrder: PropTypes.func,
  }

  static defaultProps = {
    intl: {
      formatMessage: () => {},
    },
    form: {},
    stepsProps: {
      onNext: () => {},
      onPrevious: () => {},
    },
    orderInfo: {
      name: '',
      address: '',
    },
    valueOfOrder: 0,
    onSendOrder: () => {},
  }

  handleFinish = () => {
    const {
      form,
      intl,
      onSendOrder,
      valueOfOrder,
    } = this.props;

    form.validateFields((err, values) => {
      if (err) {
        message.error(intl.formatMessage({ id: 'error_validating_additional_information' }));
        return;
      }

      const splittingValues = values.calculator.reduce((acc, curr) => {
        if (curr.value) {
          return acc + parseFloat(curr.value);
        }

        return acc;
      }, 0);

      if (splittingValues !== 0 && splittingValues.toFixed(2) !== valueOfOrder.toFixed(2)) {
        message.error(intl.formatMessage({ id: 'splitting_value_wrong' }));
        return;
      }

      onSendOrder({ ...values, valueOfOrder });
      message.success(intl.formatMessage({ id: 'steps_done_message' }));
    });
  }

  handlePrevious = () => {
    const { onPrevious } = this.props.stepsProps;
    onPrevious();
  }

  handleCalculatorValueChange = (target, value) => {
    const { form, valueOfOrder } = this.props;
    let percentValue;

    if (value) {
      percentValue = parseFloat((value / valueOfOrder) * 100).toFixed(2);
    }

    form.setFieldsValue({ [target]: percentValue });
    return value;
  }

  handleCalculatorPercentChange = (target, percentValue) => {
    const { form, valueOfOrder } = this.props;
    let value;

    if (percentValue) {
      value = parseFloat((percentValue * valueOfOrder) / 100).toFixed(2);
    }

    form.setFieldsValue({ [target]: value });
    return percentValue;
  }

  renderCalculator = () => {
    const { form, intl } = this.props;
    const { getFieldDecorator } = form;

    const amountOfPeople = form.getFieldValue('amount_of_people') || 0;

    return [...Array(amountOfPeople).keys()].map((value) => {
      const index = value + 1;
      const valueInputIdentifier = `calculator[${value}][value]`;
      const percentInputIdentifier = `calculator[${value}][percent]`;

      const valueComponent = (
        <InputNumber
          placeholder={intl.formatMessage({ id: 'value' })}
          min={0}
          className={styles.inputFullWidth}
          onChange={newValue => this.handleCalculatorValueChange(percentInputIdentifier, newValue)}
        />
      );

      const percentComponent = (
        <InputNumber
          placeholder="%"
          min={0}
          className={styles.inputFullWidth}
          onChange={newValue => this.handleCalculatorPercentChange(valueInputIdentifier, newValue)}
        />
      );

      return (
        <Row key={value}>
          <Col
            xs={{ span: 24 }}
            lg={{ span: 6 }}
          >
            <FormItem
              className="align-label-left"
              label={intl.formatMessage({ id: 'person_number' }, { number: index })}
              labelCol={{ span: 6 }}
              wrapperCol={{ span: 18 }}
            >
              {getFieldDecorator(valueInputIdentifier)(valueComponent)}
            </FormItem>
          </Col>
          <Col
            xs={{ span: 24 }}
            lg={{ span: 6, offset: 2 }}
          >
            <FormItem
              wrapperCol={{ span: 18 }}
            >
              {getFieldDecorator(percentInputIdentifier)(percentComponent)}
            </FormItem>
          </Col>
        </Row>
      );
    });
  }

  renderAmountPerPeople = () => {
    const { form, valueOfOrder } = this.props;
    const amountOfPeople = form.getFieldValue('amount_of_people');
    let value = valueOfOrder;

    if (amountOfPeople) {
      value = parseFloat(valueOfOrder / amountOfPeople);
    }

    return `R$ ${value.toFixed(2)}`;
  }

  render() {
    const {
      stepsProps,
      form,
      orderInfo,
      valueOfOrder,
      intl,
    } = this.props;

    const { getFieldDecorator } = form;

    const paymentComponent = (
      <RadioGroup>
        <Radio value="Crédito">
          <FormattedMessage id="credit" />
        </Radio>
        <Radio value="Débito">
          <FormattedMessage id="debit" />
        </Radio>
        <Radio value="Dinheiro">
          <FormattedMessage id="money" />
        </Radio>
      </RadioGroup>
    );

    const paymentChangeComponent = (
      <RadioGroup>
        <Radio value="Sim" >
          <FormattedMessage id="yes" />
        </Radio>
        <Radio value="Não">
          <FormattedMessage id="no" />
        </Radio>
      </RadioGroup>
    );

    return (
      <Fragment>
        <StepsBody>
          <Form layout="horizontal">
            <Row type="flex">
              <Col
                xs={{ span: 24 }}
                lg={{ span: 12 }}
                className={cx(styles.cardsGrid, styles.cardContainer)}
              >
                <Card
                  className={cx('order-review', styles.marginBottom20)}
                  title={intl.formatMessage({ id: 'card_title_review' })}
                >
                  <Row>
                    <Col>
                      <Avatar size="large" icon="user" />
                      <span className={styles.avatarText}>{orderInfo.name}</span>
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                      <Avatar size="large" icon="home" />
                      <span className={styles.avatarText}>{orderInfo.address}</span>
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                      <Avatar size="large" >R$</Avatar>
                      <span className={styles.avatarText}>{valueOfOrder.toFixed(2)}</span>
                    </Col>
                  </Row>
                </Card>
                <Card title={intl.formatMessage({ id: 'card_title_payment' })}>
                  <FormItem label={intl.formatMessage({ id: 'what_is_the_payment_method?' })}>
                    {getFieldDecorator('payment_type', {
                      rules: [{
                        required: true, message: intl.formatMessage({ id: 'this_fields_is_required_message' }),
                      }],
                    })(paymentComponent)}
                  </FormItem>
                  <FormItem label={intl.formatMessage({ id: 'need_change?' })} >
                    {getFieldDecorator('need_change', {
                      rules: [{
                        required: true, message: intl.formatMessage({ id: 'this_fields_is_required_message' }),
                      }],
                    })(paymentChangeComponent)}
                  </FormItem>
                </Card>
              </Col>
              <Col xs={{ span: 24 }} lg={{ span: 12 }} className={styles.cardContainer}>
                <Card className={cx('order-review', styles.fullHeight)} title={intl.formatMessage({ id: 'card_title_helper' })}>
                  <Row>
                    <Col><FormattedMessage id="your_order_is_already_in_progress" /></Col>
                  </Row>
                  <Row type="flex" justify="space-between">
                    <Col span={12}><FormattedMessage id="value_of_the_order" /></Col>
                    <Col span={12} className={styles.textRight}>R$ {valueOfOrder.toFixed(2)}</Col>
                  </Row>
                  <Row>
                    <Col>
                      <FormItem className="align-label-left" label={intl.formatMessage({ id: 'amount_of_people' })} labelCol={{ span: 4 }}>
                        {getFieldDecorator('amount_of_people', { initialValue: 1 })(<InputNumber min={1} />)}
                      </FormItem>
                    </Col>
                  </Row>
                  <Row>
                    <Col span={12}><FormattedMessage id="value_per_person" /></Col>
                    <Col span={12} className={styles.textRight}>{this.renderAmountPerPeople()}</Col>
                  </Row>
                  <Divider />
                  <Row>
                    <Col>
                      <FormattedMessage id="use_our_calculator_if_you_want_to_split" />
                    </Col>
                  </Row>
                  {this.renderCalculator()}
                </Card>
              </Col>
            </Row>
          </Form>
        </StepsBody>
        <StepsFooter {...stepsProps} onDone={this.handleFinish} onPrevious={this.handlePrevious} />
      </Fragment>
    );
  }
}

export default Form.create()(connect(
  ({ entities, communication }) => {
    const {
      loading,
    } = communication.of(CONTROL_ORDER_INFO);

    if (loading) {
      return {};
    }

    const ingredientIds = entities.idsOf('ingredients');
    const ingredients = ingredientIds.map(ingredientId => entities.contentOf('ingredients', ingredientId));

    const snackIds = entities.idsOf(CONTROL_ORDER_CART);
    const snacks = snackIds.map(snackId => entities.contentOf(CONTROL_ORDER_CART, snackId));
    const valueOfOrder = calculateTotal(snacks, ingredients);

    const orderInfo = entities.contentOf(CONTROL_ORDER_INFO, GENERIC_ID);

    return { orderInfo, valueOfOrder };
  },
  { onSendOrder: sendOrder },
)(injectIntl(OrderReviewSection)));
