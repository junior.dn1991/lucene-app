import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
  Modal,
  Form,
  Select,
  Row,
  Col,
  Checkbox,
} from 'antd';

import { injectIntl, FormattedMessage } from 'react-intl';

import {
  INGREDIENT_CATEGORY_BREAD,
  INGREDIENT_CATEGORY_CHEESE,
  INGREDIENT_CATEGORY_FILLING,
  INGREDIENT_CATEGORY_SAUCES,
  INGREDIENT_CATEGORY_SALAD,
  INGREDIENT_CATEGORY_SPICE,
} from 'constants/ingredients';

import { addSnackToCart, updateSnackOnCart } from 'actions/controlActions';

import { calculateSnackValue } from 'utils/cartUtils';

import ValueInfoCard from 'components/shared/ValueInfoCard';

import styles from './CartModalForm.css';

const FormItem = Form.Item;
const Option = Select.Option; // eslint-disable-line prefer-destructuring
const CheckboxGroup = Checkbox.Group;

class CartModalForm extends Component {
  static propTypes = {
    intl: PropTypes.shape({
      formatMessage: PropTypes.func,
    }),
    breadTypes: PropTypes.arrayOf(PropTypes.shape({})),
    cheeseTypes: PropTypes.arrayOf(PropTypes.shape({})),
    editingSnack: PropTypes.shape({}),
    fillingTypes: PropTypes.arrayOf(PropTypes.shape({})),
    form: PropTypes.shape({
      getFieldDecorator: PropTypes.func.required,
      setFieldsValue: PropTypes.func.required,
    }),
    ingredients: PropTypes.arrayOf(PropTypes.shape({})),
    onAddSnackToCart: PropTypes.func,
    onCloseModal: PropTypes.func,
    onUpdateSnackOnCart: PropTypes.func,
    saladTypes: PropTypes.arrayOf(PropTypes.shape({})),
    sauceTypes: PropTypes.arrayOf(PropTypes.shape({})),
    showModal: PropTypes.bool,
    snacks: PropTypes.arrayOf(PropTypes.shape({})),
    spiceTypes: PropTypes.arrayOf(PropTypes.shape({})),
  };

  static defaultProps = {
    intl: {
      formatMessage: () => {},
    },
    breadTypes: [],
    cheeseTypes: [],
    editingSnack: {},
    fillingTypes: [],
    form: {},
    ingredients: [],
    onAddSnackToCart: () => {},
    onCloseModal: () => {},
    onUpdateSnackOnCart: () => {},
    saladTypes: [],
    sauceTypes: [],
    showModal: false,
    snacks: [],
    spiceTypes: [],
  }

  handleSnackChange = (value) => {
    const { snacks, form } = this.props;
    const snack = snacks.find(currentSnack => currentSnack.name === value);
    if (snack) {
      const values = snack.ingredients.reduce((acc, curr) => {
        if (curr.category === INGREDIENT_CATEGORY_BREAD) {
          return { ...acc, typeOfBread: curr.name };
        }

        if (curr.category === INGREDIENT_CATEGORY_CHEESE) {
          return { ...acc, cheese: curr.name };
        }

        if (curr.category === INGREDIENT_CATEGORY_FILLING) {
          return { ...acc, filling: curr.name };
        }

        if (curr.category === INGREDIENT_CATEGORY_SAUCES) {
          return { ...acc, sauces: acc.sauces.concat(curr.name) };
        }

        if (curr.category === INGREDIENT_CATEGORY_SALAD) {
          return { ...acc, salad: acc.salad.concat(curr.name) };
        }

        if (curr.category === INGREDIENT_CATEGORY_SPICE) {
          return { ...acc, spice: acc.spice.concat(curr.name) };
        }

        return acc;
      }, {
        typeOfBread: undefined,
        cheese: undefined,
        filling: undefined,
        salad: [],
        sauces: [],
        spice: [],
        double: [],
      });

      form.setFieldsValue(values);
    } else {
      form.resetFields();
    }
  }

  handleDoubleChange = (target, values) => {
    const { form } = this.props;
    if (values.length) {
      const fieldValues = form.getFieldValue(target);

      const newFieldvalues = values.reduce((acc, curr) => {
        if (!acc.includes(curr)) {
          return acc.concat(curr);
        }

        return acc;
      }, fieldValues);

      form.setFieldsValue({ [target]: newFieldvalues });
    }

    return values;
  }

  handleOptionsChange = (target, values) => {
    const { form } = this.props;
    const doubleValues = form.getFieldValue(target);

    const newFieldvalues = values.reduce((acc, curr) => {
      if (doubleValues.includes(curr)) {
        return acc.concat(curr);
      }

      return acc;
    }, []);

    form.setFieldsValue({ [target]: newFieldvalues });

    return values;
  }

  handleSave = () => {
    const {
      form,
      onAddSnackToCart,
      onUpdateSnackOnCart,
      editingSnack,
    } = this.props;

    form.validateFields((err, values) => {
      if (err) {
        return;
      }

      if (editingSnack.id) {
        onUpdateSnackOnCart({ ...values, id: editingSnack.id });
      } else {
        onAddSnackToCart(values);
      }

      this.handleCloseModal();
    });
  }

  handleCloseModal = () => {
    const {
      form,
      onCloseModal,
    } = this.props;

    form.resetFields();
    onCloseModal();
  }

  renderSelect = ({
    items,
    key,
    ...others
  }) => (
    <Select
      showSearch
      optionFilterProp="children"
      filterOption={(input, option) =>
        option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
      {...others}
    >
      {items.map(item => (
        <Option key={item[key]} value={item[key]}>
          <FormattedMessage id={item[key]} />
        </Option>
      ))}
    </Select>
  );

  renderCheckboxGroup = ({
    items,
    key,
    text,
    ...others
  }) => (
    <CheckboxGroup className={styles.fullWidth} {...others} >
      <Row>
        {items.map(item => (
          <Col key={item[key]} xs={24} lg={24}>
            <Checkbox value={item[key]}>
              <FormattedMessage id={text || item[key]} />
            </Checkbox>
          </Col>
        ))}
      </Row>
    </CheckboxGroup>
  );

  render() {
    const {
      intl,
      breadTypes,
      cheeseTypes,
      editingSnack,
      ingredients,
      fillingTypes,
      form,
      saladTypes,
      sauceTypes,
      showModal,
      snacks,
      spiceTypes,
    } = this.props;

    const { getFieldDecorator } = form;

    return (
      <Modal
        visible={showModal}
        title={intl.formatMessage({ id: 'snack_selection' })}
        okText={intl.formatMessage({ id: 'save' })}
        width={800}
        onCancel={this.handleCloseModal}
        onOk={this.handleSave}
      >
        <Form layout="horizontal">
          <FormItem label={intl.formatMessage({ id: 'list_of_snacks' })}>
            {getFieldDecorator('name', {
              initialValue: editingSnack.name,
            })(this.renderSelect({
              items: snacks,
              key: 'name',
              placeholder: intl.formatMessage({ id: 'snack_cart_placeholder' }),
              allowClear: true,
              onChange: (value) => {
                this.handleSnackChange(value);
                return value;
              },
            }))}
          </FormItem>
          <Row gutter={24}>
            <Col xs={24} lg={8}>
              <FormItem label={intl.formatMessage({ id: 'type_of_bread' })}>
                {
                  getFieldDecorator('typeOfBread', {
                    initialValue: editingSnack.typeOfBread,
                    rules: [
                      { required: true, message: intl.formatMessage({ id: 'this_fields_is_required_message' }) },
                    ],
                  })(this.renderSelect({ items: breadTypes, key: 'name', placeholder: intl.formatMessage({ id: 'bread_cart_placeholder' }) }))
                }
              </FormItem>
            </Col>
            <Col xs={24} lg={8}>
              <FormItem label={intl.formatMessage({ id: 'cheese' })}>
                {
                  getFieldDecorator('cheese', {
                    initialValue: editingSnack.cheese,
                  })(this.renderSelect({ items: cheeseTypes, key: 'name', placeholder: intl.formatMessage({ id: 'cheese_cart_placeholder' }) }))
                }
              </FormItem>
            </Col>
            <Col xs={24} lg={8}>
              <FormItem label={intl.formatMessage({ id: 'filling' })}>
                {
                  getFieldDecorator('filling', {
                    initialValue: editingSnack.filling,
                  })(this.renderSelect({ items: fillingTypes, key: 'name', placeholder: intl.formatMessage({ id: 'filling_cart_placeholder' }) }))
                }
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <FormItem label={intl.formatMessage({ id: 'salad' })}>
                {
                  getFieldDecorator('salad', {
                    initialValue: editingSnack.salad || [],
                  })(this.renderCheckboxGroup({
                    items: saladTypes,
                    key: 'name',
                    onChange: value => this.handleOptionsChange('doubleSalad', value),
                  }))
                }
              </FormItem>
            </Col>
            <Col span={12} className={styles.doubleOptionGrid}>
              <FormItem>
                {
                  getFieldDecorator('doubleSalad', {
                    initialValue: editingSnack.doubleSalad || [],
                  })(this.renderCheckboxGroup({
                    items: saladTypes,
                    key: 'name',
                    text: 'double',
                    onChange: value => this.handleDoubleChange('salad', value),
                  }))
                }
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <FormItem label={intl.formatMessage({ id: 'sauces' })}>
                {
                  getFieldDecorator('sauces', {
                    initialValue: editingSnack.sauces || [],
                  })(this.renderCheckboxGroup({
                    items: sauceTypes,
                    key: 'name',
                    onChange: value => this.handleOptionsChange('doubleSauces', value),
                  }))
                }
              </FormItem>
            </Col>
            <Col span={12} className={styles.doubleOptionGrid}>
              <FormItem>
                {
                  getFieldDecorator('doubleSauces', {
                    initialValue: editingSnack.doubleSalad || [],
                  })(this.renderCheckboxGroup({
                    items: sauceTypes,
                    key: 'name',
                    text: 'double',
                    onChange: value => this.handleDoubleChange('sauces', value),
                  }))
                }
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <FormItem label={intl.formatMessage({ id: 'spices' })}>
                {
                  getFieldDecorator('spice', {
                    initialValue: editingSnack.spice || [],
                  })(this.renderCheckboxGroup({
                    items: spiceTypes,
                    key: 'name',
                    onChange: value => this.handleOptionsChange('doubleSpice', value),
                  }))
                }
              </FormItem>
            </Col>
            <Col span={12} className={styles.doubleOptionGrid}>
              <FormItem>
                {
                  getFieldDecorator('doubleSpice', {
                    initialValue: editingSnack.doubleSalad || [],
                  })(this.renderCheckboxGroup({
                    items: spiceTypes,
                    key: 'name',
                    text: 'double',
                    onChange: value => this.handleDoubleChange('spice', value),
                  }))
                }
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <ValueInfoCard
                decimal
                value={calculateSnackValue(form.getFieldsValue(), ingredients)}
              />
            </Col>
          </Row>
        </Form>
      </Modal>
    );
  }
}

export default connect(
  ({ entities }) => {
    const snackIds = entities.idsOf('snacks');
    const snacks = snackIds.map(snackId => entities.contentOf('snacks', snackId));

    const ingredientIds = entities.idsOf('ingredients');
    const ingredients = ingredientIds.map(ingredientId => entities.contentOf('ingredients', ingredientId));

    const breadTypes = ingredients.filter(ingredient =>
      ingredient.category === INGREDIENT_CATEGORY_BREAD);

    const cheeseTypes = ingredients.filter(ingredient =>
      ingredient.category === INGREDIENT_CATEGORY_CHEESE);

    const fillingTypes = ingredients.filter(ingredient =>
      ingredient.category === INGREDIENT_CATEGORY_FILLING);

    const sauceTypes = ingredients.filter(ingredient =>
      ingredient.category === INGREDIENT_CATEGORY_SAUCES);

    const saladTypes = ingredients.filter(ingredient =>
      ingredient.category === INGREDIENT_CATEGORY_SALAD);

    const spiceTypes = ingredients.filter(ingredient =>
      ingredient.category === INGREDIENT_CATEGORY_SPICE);

    return {
      breadTypes,
      cheeseTypes,
      fillingTypes,
      ingredients,
      saladTypes,
      sauceTypes,
      snacks,
      spiceTypes,
    };
  },
  {
    onAddSnackToCart: addSnackToCart,
    onUpdateSnackOnCart: updateSnackOnCart,
  },
)(Form.create()(injectIntl(CartModalForm)));
