import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';

import PropTypes from 'prop-types';
import {
  Form,
  Input,
  Row,
  Col,
} from 'antd';

import { injectIntl } from 'react-intl';

import {
  CONTROL_ORDER_INFO,
  GENERIC_ID,
  sendSnacks,
} from 'actions/controlActions';

import StepsBody from 'components/shared/StepsBody';
import StepsFooter from 'components/shared/StepsFooter';

import styles from './OrderUserSection.css'; // eslint-disable-line no-unused-vars

const FormItem = Form.Item;

class OrderUserSection extends Component {
  static propTypes = {
    intl: PropTypes.shape({
      formatMessage: PropTypes.func,
    }),
    form: PropTypes.shape({
      getFieldDecorator: PropTypes.func.required,
      setFieldsValue: PropTypes.func.required,
    }),
    stepsProps: PropTypes.shape({
      onNext: PropTypes.func,
      onPrevious: PropTypes.func,
    }),
    orderInfo: PropTypes.shape({}),
    onSendSnacks: PropTypes.func,
  }

  static defaultProps = {
    intl: {
      formatMessage: () => {},
    },
    form: {},
    stepsProps: {
      onNext: () => {},
      onPrevious: () => {},
    },
    orderInfo: {},
    onSendSnacks: () => {},
  }

  handleNext = () => {
    const {
      form,
      onSendSnacks,
      stepsProps,
    } = this.props;

    const { onNext } = stepsProps;

    form.validateFields((err, values) => {
      if (err) {
        return;
      }

      onSendSnacks(values);
      form.resetFields();
      onNext();
    });
  }

  handlePrevious = () => {
    const { onPrevious } = this.props.stepsProps;
    onPrevious();
  }

  render() {
    const {
      form,
      stepsProps,
      orderInfo,
      intl,
    } = this.props;

    const { getFieldDecorator } = form;

    return (
      <Fragment>
        <StepsBody>
          <Form layout="horizontal">
            <Row gutter={24}>
              <Col span={24}>
                <FormItem label={intl.formatMessage({ id: 'name' })} >
                  {getFieldDecorator('name', {
                    initialValue: orderInfo.name,
                    rules: [{
                      max: 75,
                    }, {
                      required: true, message: intl.formatMessage({ id: 'this_fields_is_required_message' }),
                    }],
                  })(<Input />)}
                </FormItem>
                <FormItem label={intl.formatMessage({ id: 'address' })} >
                  {getFieldDecorator('address', {
                    initialValue: orderInfo.address,
                    rules: [{
                      max: 100,
                    }, {
                      required: true, message: intl.formatMessage({ id: 'this_fields_is_required_message' }),
                    }],
                  })(<Input />)}
                </FormItem>
              </Col>
            </Row>
          </Form>
        </StepsBody>
        <StepsFooter {...stepsProps} onNext={this.handleNext} onPrevious={this.handlePrevious} />
      </Fragment>
    );
  }
}

export default Form.create()(connect(
  ({ entities, communication }) => {
    const {
      loading,
    } = communication.of(CONTROL_ORDER_INFO);

    if (loading) {
      return {};
    }

    const orderInfo = entities.contentOf(CONTROL_ORDER_INFO, GENERIC_ID);

    return {
      orderInfo,
    };
  },
  { onSendSnacks: sendSnacks },
)(injectIntl(OrderUserSection)));
