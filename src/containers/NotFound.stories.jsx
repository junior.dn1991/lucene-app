import React from 'react';
import { storiesOf } from '@storybook/react';
import { LocaleProvider } from 'components/shared/LocaleProvider';

import NotFound from './NotFound';

storiesOf('NotFound', module)
  .add('default', () => <LocaleProvider><NotFound /></LocaleProvider>);

