import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Steps } from 'antd';
import { connect } from 'react-redux';

import { injectIntl } from 'react-intl';

import { fetchIngredients } from 'actions/ingredientsActions';
import { fetchSnacks } from 'actions/snacksActions';

import OrderReviewSection from './OrderReviewSection';
import OrderCartSection from './OrderCartSection';
import OrderUserSection from './OrderUserSection';

import styles from './OrderPage.css';

const { Step } = Steps;

class OrderPage extends Component {
  static propTypes = {
    intl: PropTypes.shape({
      formatMessage: PropTypes.func,
    }),
    onFetchIngredients: PropTypes.func,
    onFetchSnacks: PropTypes.func,
  }

  static defaultProps = {
    intl: {
      formatMessage: () => {},
    },
    onFetchIngredients: () => {},
    onFetchSnacks: () => {},
  }

  constructor(props) {
    super(props);
    this.state = {
      current: 0,
    };
  }

  componentDidMount() {
    this.props.onFetchIngredients();
    this.props.onFetchSnacks();
  }

  handleNext = () => {
    const current = this.state.current + 1;
    this.setState({ current });
  }

  handlePrev = () => {
    const current = this.state.current - 1;
    this.setState({ current });
  }

  render() {
    const { current } = this.state;
    const { intl } = this.props;

    const STEPS_INFO = intl.formatMessage({ id: 'steps_info_title' });
    const STEPS_CART = intl.formatMessage({ id: 'steps_cart_title' });
    const STEPS_REVIEW_ORDER = intl.formatMessage({ id: 'steps_review_order_title' });

    const steps = [
      { title: STEPS_CART },
      { title: STEPS_INFO },
      { title: STEPS_REVIEW_ORDER },
    ];

    const stepsProps = {
      steps: steps.length,
      current,
      onNext: this.handleNext,
      onPrevious: this.handlePrev,
    };

    let content;

    switch (steps[this.state.current].title) {
      case STEPS_INFO:
        content = <OrderUserSection stepsProps={stepsProps} />;
        break;
      case STEPS_REVIEW_ORDER:
        content = <OrderReviewSection stepsProps={stepsProps} />;
        break;
      default:
        content = <OrderCartSection stepsProps={stepsProps} />;
        break;
    }

    return (
      <div className={styles.orderPage}>
        <Steps current={current}>
          {steps.map(item => <Step key={item.title} title={item.title} />)}
        </Steps>
        {content}
      </div>
    );
  }
}

export default connect(
  null,
  {
    onFetchIngredients: fetchIngredients,
    onFetchSnacks: fetchSnacks,
  },
)(injectIntl(OrderPage));
