import axios from 'axios';
import { entities, communication, normalize } from 'redux-shelf';
import normalizeSnacks from 'normalizers/snacksNormalizer';

import { baseMSUrl } from 'constants/config';

export function fetchSnacks() {
  return async (dispatch) => {
    dispatch(communication.starting('snacks'));

    try {
      const response = await axios.get(`${baseMSUrl}/lanches`);

      dispatch(entities.set('snacks', normalize(normalizeSnacks(response.data), 'name')));
      dispatch(communication.done('snacks'));
    } catch (error) {
      dispatch(communication.fail('snacks', error.message));
    }
  };
}

export default { fetchSnacks };
