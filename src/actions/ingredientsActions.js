import axios from 'axios';
import { entities, communication, normalize } from 'redux-shelf';
import { normalizeIngredientsByCategories } from 'normalizers/ingredientsNormalizer';

import { baseMSUrl } from 'constants/config';

export function fetchIngredients() {
  return async (dispatch) => {
    dispatch(communication.starting('ingredients'));

    try {
      const response = await axios.get(`${baseMSUrl}/ingredientes`);

      dispatch(entities.set('ingredients', normalize(normalizeIngredientsByCategories(response.data), 'name')));
      dispatch(communication.done('ingredients'));
    } catch (error) {
      dispatch(communication.fail('ingredients', error.message));
    }
  };
}

export default { fetchIngredients };
