import axios from 'axios';
import { entities, communication, normalize } from 'redux-shelf';
import uniqid from 'uniqid';
import store from 'store';

import { baseMSUrl } from 'constants/config';

import { normalizeSnacksToPayload, normalizeSplits } from 'utils/cartUtils';

export const CONTROL_ORDER_CART = 'control:order:cart';
export const CONTROL_ORDER_INFO = 'control:order:info';
export const CONTROL_ORDER_FINISH = 'control:order:finish';

export const CONTROL_LANGUAGE = 'CONTROL_LANGUAGE';

export const GENERIC_ID = 1;

export function addSnackToCart(snack) {
  return (dispatch) => {
    const id = uniqid();
    dispatch(communication.starting(CONTROL_ORDER_CART, id));

    try {
      dispatch(entities.update(CONTROL_ORDER_CART, normalize({ ...snack, id })));
      dispatch(communication.done(CONTROL_ORDER_CART, id));
    } catch (error) {
      dispatch(communication.fail(CONTROL_ORDER_CART, id, error.message));
    }
  };
}

export function updateSnackOnCart(snack) {
  return (dispatch) => {
    dispatch(communication.starting(CONTROL_ORDER_CART, snack.id));

    try {
      dispatch(entities.update(CONTROL_ORDER_CART, normalize(snack)));
      dispatch(communication.done(CONTROL_ORDER_CART, snack.id));
    } catch (error) {
      dispatch(communication.fail(CONTROL_ORDER_CART, snack.id, error.message));
    }
  };
}

export function removeSnackOfCart(snackId) {
  return (dispatch) => {
    dispatch(communication.starting(CONTROL_ORDER_CART, snackId));

    try {
      dispatch(entities.remove(CONTROL_ORDER_CART, snackId));
      dispatch(communication.done(CONTROL_ORDER_CART, snackId));
    } catch (error) {
      dispatch(communication.fail(CONTROL_ORDER_CART, snackId, error.message));
    }
  };
}

export function changeLanguage(code) {
  return (dispatch) => {
    dispatch(communication.starting(CONTROL_LANGUAGE));

    try {
      dispatch(entities.update(CONTROL_LANGUAGE, normalize({ code, id: GENERIC_ID })));
      store.set('language', code);
      dispatch(communication.done(CONTROL_LANGUAGE));
    } catch (error) {
      dispatch(communication.fail(CONTROL_LANGUAGE, error.message));
    }
  };
}

export function sendSnacks(orderInfo) {
  return async (dispatch, getState) => {
    dispatch(communication.starting(CONTROL_ORDER_INFO));

    try {
      const { entities: entitiesState } = getState();

      const snackIds = entitiesState.idsOf(CONTROL_ORDER_CART);
      const snacks = snackIds.map(snackId => entitiesState.contentOf(CONTROL_ORDER_CART, snackId));

      const ingredientIds = entitiesState.idsOf('ingredients');
      const ingredients = ingredientIds.map(ingredientId => entitiesState.contentOf('ingredients', ingredientId));

      const payload = {
        nome: orderInfo.name,
        endereco: orderInfo.address,
        lanches: normalizeSnacksToPayload(snacks, ingredients),
      };

      const response = await axios.post(`${baseMSUrl}/lanches`, payload);
      const orderId = response.data['_id-pedido'];
      const normalizedResponse = normalize({ ...orderInfo, orderId, id: GENERIC_ID });

      dispatch(entities.update(CONTROL_ORDER_INFO, normalizedResponse));
      dispatch(communication.done(CONTROL_ORDER_INFO));
    } catch (error) {
      dispatch(communication.fail(CONTROL_ORDER_INFO, error.message));
    }
  };
}

export function sendOrder(additionalInformation) {
  return async (dispatch, getState) => {
    dispatch(communication.starting(CONTROL_ORDER_INFO));

    try {
      const { entities: entitiesState } = getState();

      const orderInfo = entitiesState.contentOf(CONTROL_ORDER_INFO, GENERIC_ID);

      const payload = {
        'forma-pagamento': additionalInformation.payment_type,
        troco: additionalInformation.need_change,
        pessoas: normalizeSplits(additionalInformation),
      };

      await axios.post(`${baseMSUrl}/pedidos/${orderInfo.orderId}`, payload);
      const updatedOrderInfo = normalize({ ...orderInfo, ...additionalInformation });

      dispatch(entities.update(CONTROL_ORDER_INFO, updatedOrderInfo));
      dispatch(communication.done(CONTROL_ORDER_INFO));
    } catch (error) {
      dispatch(communication.fail(CONTROL_ORDER_INFO, error.message));
    }
  };
}
