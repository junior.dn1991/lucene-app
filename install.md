## Installation
`sudo apt-get update`
`sudo apt-get install nodejs`
`sudo apt-get install npm`
`npm install -g yarn`
`yarn install`

## Run
`yarn start`

Then open [http://localhost:3000](http://localhost:3000).

## Build
`yarn build`

The output will be in build folder.

## Storybook
`yarn storybook`

Then open [http://localhost:9009/](http://localhost:9009/).
